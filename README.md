
# GC Sensor Processor 

Application is designed to take output from in sensor as a text file (.txt) 
and process the data into either CSV or JSON format.

The project is licenced under MIT.

## Running 

The application can be found in "/target/" you should just be able to double click the .jar 
to run the application. It should run on Windows, Mac and Linux, although I have only tested Windows. 

The application takes a text file as input and a directory as output e.g.

Input: C:\Users\siddle.jonathan\Desktop\readings.txt

Output: C:\Users\siddle.jonathan\Desktop\

You can use the file finder or enter a file location manually.

Examples of input and output can be found in the "/Examples" folder.

## Building 

The application use Java 1.8. The project can be built using Maven and the following: 

mvn clean compile assembly:single

You should be able to import the project into any Java IDE that supports Maven 