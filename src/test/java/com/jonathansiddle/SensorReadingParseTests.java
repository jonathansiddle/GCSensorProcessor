/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.jonathansiddle;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by : Jonathan Siddle
 * Date : 16-Sep-17
 * Description: test to make sure that a sensor reading can be parsed correctly
 */
public class SensorReadingParseTests {

  @Test
  public void canCorrectlyParseReadingIntoObject() {
    String readingInput = "I:TUSCSens1#N:0#BATTERY:98#TIME:Fri, 17/09/15, 11:15:53#Ultrasound:45.36#Humidity:-25.80#Luminosity:0.00#Noise:50.00#Dust:0.000";

    SensorReading reading = SensorReading.parse(readingInput);

    //System.out.println("Test");
    assertTrue(reading.getSensorName().equals("TUSCSens1"));
    assertTrue(reading.getMessageNumber().equals("0"));
    assertTrue(reading.getBatteryPercentage().equals("98"));
    assertTrue(reading.getTimeStamp().equals("Fri-17/09/15-11:15:53"));
    assertTrue(reading.getUltraSound().equals("45.36"));
    assertTrue(reading.getHumidity().equals("-25.80"));
    assertTrue(reading.getLuminosity().equals("0.00"));
    assertTrue(reading.getNoise().equals("50.00"));
    assertTrue(reading.getDust().equals("0.000"));
    //assertTrue(false);
  }
}
