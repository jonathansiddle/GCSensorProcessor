/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.jonathansiddle;

/**
 * Created by : Jonathan Siddle
 * Date : 16-Sep-17
 * Description: Class to wrap-up the data from a sensor reading.
 */
public class SensorReading {
  //Variables
  private String sensorName;
  private String messageNumber;
  private String batteryPercentage;
  private String timeStamp;
  private String ultraSound;
  private String humidity;
  private String luminosity;
  private String noise;
  private String dust;

  //Methods
  //This is the main processing method that converts a reading
  //from a string into a Java objecct
  public static SensorReading parse(String reading) {
    //this will split the input line into properties and values
    //e.g. I:TUSCSens1#N:0#BATTERY:98 will become
    // I:TUSCSens1
    // N:0
    // BATTERY:98
    //String array starts at 0 and props are in the same
    //order the originally appear in the string
    String[] propsAndValues = reading.split("#");

    //set object props
    SensorReading r = new SensorReading();
    r.setSensorName(splitPropAndValue(propsAndValues[0]));
    r.setMessageNumber(splitPropAndValue(propsAndValues[1]));
    r.setBatteryPercentage(splitPropAndValue(propsAndValues[2]));
    r.setTimeStamp(splitPropAndValue(propsAndValues[3]));
    r.setUltraSound(splitPropAndValue(propsAndValues[4]));
    r.setHumidity(splitPropAndValue(propsAndValues[5]));
    r.setLuminosity(splitPropAndValue(propsAndValues[6]));
    r.setNoise(splitPropAndValue(propsAndValues[7]));
    r.setDust(splitPropAndValue(propsAndValues[8]));

    return r;
  }

  //Method to wrap up splitting property and value from reading
  //need to split input on the first instance of ':'
  private static String splitPropAndValue(String propAndValue) {
    int splitIndex = propAndValue.indexOf(":")+1;

    return propAndValue.substring(splitIndex, propAndValue.length()).trim();
  }

  //getter and setter
  public String getSensorName() {
    return sensorName;
  }

  public void setSensorName(String sensorName) {
    this.sensorName = sensorName;
  }

  public String getMessageNumber() {
    return messageNumber;
  }

  public void setMessageNumber(String messageNumber) {
    this.messageNumber = messageNumber;
  }

  public String getBatteryPercentage() {
    return batteryPercentage;
  }

  public void setBatteryPercentage(String batteryPercentage) {
    this.batteryPercentage = batteryPercentage;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp.replace(", ", "-");
  }

  public String getUltraSound() {
    return ultraSound;
  }

  public void setUltraSound(String ultraSound) {
    this.ultraSound = ultraSound;
  }

  public String getHumidity() {
    return humidity;
  }

  public void setHumidity(String humidity) {
    this.humidity = humidity;
  }

  public String getLuminosity() {
    return luminosity;
  }

  public void setLuminosity(String luminosity) {
    this.luminosity = luminosity;
  }

  public String getNoise() {
    return noise;
  }

  public void setNoise(String noise) {
    this.noise = noise;
  }

  public String getDust() {
    return dust;
  }

  public void setDust(String dust) {
    this.dust = dust;
  }
}
