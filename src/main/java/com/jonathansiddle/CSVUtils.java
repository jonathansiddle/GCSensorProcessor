/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.jonathansiddle;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Created by : Jonathan Siddle
 * Date : 16-Sep-17
 * Description: code to help output data to CSV file taken from :
 *  https://www.mkyong.com/java/how-to-export-data-to-csv-file-java/
 */
public class CSVUtils {

  private static final char DEFAULT_SEPARATOR = ',';

  public static void writeLine(Writer w, List<String> values) throws IOException {
    writeLine(w, values, DEFAULT_SEPARATOR, ' ');
  }

  public static void writeLine(Writer w, List<String> values, char separators) throws IOException {
    writeLine(w, values, separators, ' ');
  }

  //https://tools.ietf.org/html/rfc4180
  private static String followCVSformat(String value) {

    String result = value;
    if (result.contains("\"")) {
      result = result.replace("\"", "\"\"");
    }
    return result;

  }

  public static void writeLine(Writer w, List<String> values, char separators, char customQuote) throws IOException {

    boolean first = true;

    //default customQuote is empty

    if (separators == ' ') {
      separators = DEFAULT_SEPARATOR;
    }

    StringBuilder sb = new StringBuilder();
    for (String value : values) {
      if (!first) {
        sb.append(separators);
      }
      if (customQuote == ' ') {
        sb.append(followCVSformat(value));
      } else {
        sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
      }

      first = false;
    }
    sb.append("\n");
    w.append(sb.toString());
  }
}
