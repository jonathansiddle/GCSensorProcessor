/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.jonathansiddle;

import com.google.common.io.Files;
import com.google.gson.Gson;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by : Jonathan Siddle
 * Date : 16-Sep-17
 * Description: Main GUI class.
 */
public class MainFrame extends JFrame {

  //Constructor
  public MainFrame(String title) {
    super(title);
    init();
  }

  //Set up UI Layout and functionality
  private void init() {
    this.setLayout(new MigLayout());
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JTextField outputFile = new JTextField("");

    JPanel topPanel = new JPanel();
    topPanel.setLayout(new MigLayout());

    JLabel inputLabel = new JLabel("Input File");
    JTextField inputFile = new JTextField("");
    JButton selectInputFile = new JButton("...");
    selectInputFile.addActionListener(e -> {
      //File Locator
      JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
      FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt");
      jfc.setFileFilter(filter);
      int returnValue = jfc.showOpenDialog(null);
      if (returnValue == JFileChooser.APPROVE_OPTION) {
        File selectedFile = jfc.getSelectedFile();
        inputFile.setText(selectedFile.getAbsolutePath());
        outputFile.setText(selectedFile.getAbsolutePath().substring(0,
                selectedFile.getAbsolutePath().lastIndexOf(File.separator) + 1));
      }
    });

    //top panel setup
    topPanel.add(inputLabel, "w 30%, h 100%, growx, growy");
    topPanel.add(inputFile, "w 60%, h 100%, growx, growy");
    topPanel.add(selectInputFile, "w 10%, h 100%, growx, growy, wrap");

    JPanel bottomPanel = new JPanel();
    bottomPanel.setLayout(new MigLayout());

    JLabel outputLabel = new JLabel("Output Dir");

    //top panel setup
    topPanel.add(outputLabel, "w 30%, h 100%, growx, growy");
    topPanel.add(outputFile, "w 70%, h 100%, growx, growy");

    JPanel outputPanel = new JPanel();
    outputPanel.setLayout(new MigLayout());
    JButton outputCsv = new JButton("Output CSV");
    outputCsv.addActionListener(e -> {
      //------------------------
      // Processing to output CSV File
      //------------------------
      List<SensorReading> readings = ProcessInputData(inputFile.getText());

      String outputCSVFile = outputFile.getText() + "readings.csv";
      try {
        FileWriter writer = new FileWriter(outputCSVFile);
        //write headings row
        List<String> headings = Arrays.asList(
                "Sensor Name",
                "Message Number",
                "Battery Percentage",
                "Time Stamp",
                "Ultrasound",
                "Humidity",
                "Luminosity",
                "Noise",
                "Dust");
        CSVUtils.writeLine(writer, headings);
        //write data
        for (SensorReading reading : readings) {
          List<String> data = Arrays.asList(reading.getSensorName(),
                  reading.getMessageNumber(),
                  reading.getBatteryPercentage(),
                  reading.getTimeStamp(),
                  reading.getUltraSound(),
                  reading.getHumidity(),
                  reading.getLuminosity(),
                  reading.getNoise(),
                  reading.getDust());

          CSVUtils.writeLine(writer, data);
        }

        writer.flush();
        writer.close();
        showMessageDialog(null, "Done!");
      } catch (IOException error) {
        showMessageDialog(null, "Error writing CSV output:" + error.getMessage());
      }
    });
    JButton outputJson = new JButton("Output JSON3");
    outputJson.addActionListener(e -> {
      //------------------------
      // Processing to output JSON File
      //------------------------
      List<SensorReading> readings = ProcessInputData(inputFile.getText());

      //Process object into json format,
      //slight wrapping is to make a json list
      //of individual objects
      Gson gson = new Gson();
      StringBuilder sb = new StringBuilder();
      sb.append("{ \"readings\" : [");
      for (SensorReading reading : readings) {
        sb.append(gson.toJson(reading) + ",");
      }
      String output = sb.toString();
      output = output.substring(0, output.length()-1) + "]}";

      try {
        FileUtils.write(new File(outputFile.getText() + "readings.json"),output, "UTF-8");
        showMessageDialog(null, "Done!");
      } catch (IOException error) {
        showMessageDialog(null, "Error writing JSON output:" + error.getMessage());
      }
    });

    outputPanel.add(outputCsv, "w 50%, h 100%, growx, growy");
    outputPanel.add(outputJson, "w 50%, h 100%, growx, growy, wrap");

    //add sides to panel
    this.add(topPanel, "w 100%, h 100%, growx, growy, wrap");
    this.add(bottomPanel, "w 100%, h 100%, growx, growy, wrap");
    this.add(outputPanel, "w 100%, h 100%, growx, growy, wrap");
  }

  //Method takes file location and processing readings into a list of
  //objects
  public static List<SensorReading> ProcessInputData(String inputLocation){
    List<SensorReading> readings = new ArrayList<>();
    //read file into string, this will need to be optimised if you start
    //dealing with very large files
    try {
      List<String> fileContents = Files.readLines(new File(inputLocation), Charset.defaultCharset());
      for (String line : fileContents) {
        //make sure only lines containing readings are processed
        if (line.contains("TUSC")) {
          readings.add(SensorReading.parse(line));
        }
      }
    } catch (IOException error) {
      showMessageDialog(null, "Could not read file:" + error.getMessage());
    }

    return readings;
  }
}
